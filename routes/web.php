<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('/admin');
});

Route::screen('/admin/actual-objects/create', 'Screens\CreateObject', 'admin.actual-objects.create');
Route::screen('/admin/actual-objects/{id}/edit', 'Screens\UpdateObject', 'admin.actual-objects.edit');
Route::screen('/admin/actual-objects/{id}', 'Screens\ViewObject', 'admin.actual-objects.view');
Route::screen('/admin/archive-objects', 'Screens\ArchiveObjects', 'admin.archive-objects.index');
Route::screen('/admin/actual-objects', 'Screens\ActualObjects', 'admin.actual-objects.index');

Route::screen('/admin/actual-users/create', 'Screens\CreateUser', 'admin.actual-users.create');
Route::screen('/admin/actual-users/{id}/edit', 'Screens\UpdateUser', 'admin.actual-users.edit');
Route::screen('/admin/actual-users', 'Screens\ActualUsers', 'admin.actual-users.index');

Auth::routes();
