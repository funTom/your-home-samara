<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Platform\Filters\Filter;

class ActualUsersFilter extends Filter
{

    /**
     * @var array
     */
    public $parameters = [];

    /**
     * @var bool
     */
    public $display = true;

    /**
     * @var bool
     */
    public $dashboard = true;

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        $where = [];

        if ($this->request->filled('phone')) {
            $where[] = ['meta->phone', '=', $this->request->get('phone')];
        }
        if ($this->request->filled('dogovor_series')) {
            $where[] = ['meta->dogovor_series', '=', $this->request->get('dogovor_series')];
        }
        if ($this->request->filled('dogovor_number')) {
            $where[] = ['meta->dogovor_number', '=', $this->request->get('dogovor_number')];
        }

        if ($this->request->filled('zaselen')) {
            $where[] = ['meta->zaselen', !!$this->request->get('zaselen')];
        }
        if ($this->request->filled('zaselenme')) {
            $where[] = ['meta->zaselenme', !!$this->request->get('zaselenme')];
        }

        if ($this->request->filled('manager_id')) {
            $builder->whereRaw('meta->\'$."manager_id"\' IN (' . implode(', ', $this->request->get('manager_id')) . ')');
        }
        $builder->where($where);

        if ($this->request->filled('search')) {
            $builder->where(function ($query) {
                return $query
                    ->whereRaw('LOWER(CONCAT(meta->>\'$."last_name"\', \' \', meta->>\'$."first_name"\', \' \', meta->>\'$."middle_name"\')) LIKE \'%' . mb_strtolower($this->request->get('search')) . '%\'')
                    ->orWhereRaw('meta->>\'$."phone"\' LIKE \'%' . mb_strtolower($this->request->get('search')) . '%\'');
            });
        }
        return $builder;

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function display()
    {
        return view('simpleUserFilter', [
            'request' => $this->request,
        ]);

    }
}
