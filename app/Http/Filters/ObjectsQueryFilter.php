<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Platform\Filters\Filter;

class ObjectsQueryFilter extends Filter
{

    /**
     * @var array
     */
    public $parameters = [];

    /**
     * @var bool
     */
    public $display = true;

    /**
     * @var bool
     */
    public $dashboard = true;

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        $where = [];

        if ($this->request->filled('phone')) {
            $where[] = ['phone', '=', $this->request->get('phone')];
        }

        if ($this->request->filled('type_id')) {
            $where[] = ['type_id', '=', $this->request->get('type_id')];
        }

        if ($this->request->filled('price_from')) {
            $where[] = ['price', '>=', $this->request->get('price_from')];
        }

        if ($this->request->filled('price_to')) {
            $where[] = ['price', '<=', $this->request->get('price_to')];
        }

        if ($this->request->filled('series')) {
            $where[] = ['series', '=', $this->request->get('series')];
        }
        if ($this->request->filled('square_from')) {
            $where[] = ['square', '>=', $this->request->get('square_from')];
        }

        if ($this->request->filled('square_to')) {
            $where[] = ['square', '<=', $this->request->get('square_to')];
        }

        if ($this->request->filled('furniture')) {
            $where[] = ['furniture', '=', !!$this->request->get('furniture')];
        }

        if ($this->request->filled('remonttype_id')) {
            $where[] = ['remonttype_id', '=', $this->request->get('remonttype_id')];
        }

        if ($this->request->filled('area_id')) {
            $where[] = ['area_id', '=', $this->request->get('area_id')];
        }

        if ($this->request->filled('phone')) {
            $where[] = ['phone', '=', $this->request->get('phone')];
        }

        if ($this->request->filled('manager_id')) {
            $where[] = ['manager_id', '=', $this->request->get('manager_id')];
        }

        if ($this->request->filled('id')) {
            $where[] = ['id', '=', $this->request->get('id')];
        }

        if ($this->request->filled('no')) {
            $where[] = ['no', '=', !!$this->request->get('no')];
        }

        if ($this->request->filled('nd')) {
            $where[] = ['nd', '=', !!$this->request->get('nd')];
        }

        $builder->where($where);

        if ($this->request->filled('search')) {
            $builder->where(function ($query) {
                //"select * from `actual_objects` where (`no` = ?) and (LOWER(fio) LIKE '%кирова%' or `address`->'$."name"' LIKE ?)"
                return $query
                    ->whereRaw('LOWER(fio) LIKE ' . '\'%' . mb_strtolower($this->request->get('search')) . '%\'')
                    ->orWhereRaw('LOWER(address->\'$."name"\') LIKE \'%' . mb_strtolower($this->request->get('search')) . '%\'');
            });
        }
        return $builder;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function display()
    {
        return view('simpleFilter', [
            'request' => $this->request,
        ]);
    }
}
