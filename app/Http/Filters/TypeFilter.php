<?php

namespace App\Http\Filters;

use App\Types;
use Illuminate\Database\Eloquent\Builder;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Filters\Filter;

class TypeFilter extends Filter
{

    /**
     * @var array
     */
    public $parameters = ['type_id'];

    /**
     * @var bool
     */
    public $display = true;

    /**
     * @var bool
     */
    public $dashboard = true;

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('type_id', $this->request->get('type_id'));

    }

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function display()
    {
        return Field::tag('select')
            ->options($this->prepareSelect(Types::class))
            ->name('type_id')
            ->title('Тип недвижимости');

    }
}
