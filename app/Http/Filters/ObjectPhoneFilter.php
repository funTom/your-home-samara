<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Filters\Filter;

class ObjectPhoneFilter extends Filter
{

    /**
     * @var array
     */
    public $parameters = ['phone'];

    /**
     * @var bool
     */
    public $display = true;

    /**
     * @var bool
     */
    public $dashboard = true;

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('phone', $this->request->get('phone'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function display()
    {
        return Field::tag('input')
            ->value($this->request->get('phone'))
            ->name('phone')
            ->mask('+7 (999) 999-9999')
            ->title('Номер телефона');

    }
}
