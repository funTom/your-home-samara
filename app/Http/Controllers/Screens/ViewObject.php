<?php

namespace App\Http\Controllers\Screens;

use App\ActualObjects;
use App\GeoCode;
use App\Layouts\CreateObjectLayoutView;
use App\Layouts\LeftPartView;
use App\Layouts\RightPartView;
use Illuminate\Http\Request;
use Orchid\Platform\Facades\Alert;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class ViewObject extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Просмотр объекта';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Просмотр объекта';

    /**
     * Query data
     *
     * @return array
     */
    public function query($id = null): array
    {
        $object = ActualObjects::with('attachment')->firstOrNew(['id' => $id]);
        if ($object->address && (!$object->address['lat'] || !$object->address['lng'])) {
            $address = $object->address;
            $geo = new GeoCode;
            $geocode = $geo->google_maps_search($object->address['name'] . ', Самара');
            $address['lat'] = $geocode['lat'];
            $address['lng'] = $geocode['lng'];
            $object->address = $address;
            $object->update();
        }
        return [
            'object' => $object,
            'photos' => $object->attachment,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Link::name('назад')->link(redirect()->back()->getTargetUrl()),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            Layouts::columns([
                'left' => [
                    LeftPartView::class,
                ],
                'right' => [
                    RightPartView::class,
                ],
            ]),
            CreateObjectLayoutView::class,

        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $object = $request->get('object');
        $object['furniture'] = $request->has('object.furniture');
        $model = ActualObjects::updateOrCreate([
            'id' => $request->id ?? null,
        ], $object);
        $model->attachment()
            ->attach($request->get('photos'), [
                'attachmentable_group' => 'photos',
            ]);
        Alert::info('Объект успешно сохранен');
        return redirect()->back();
    }
}
