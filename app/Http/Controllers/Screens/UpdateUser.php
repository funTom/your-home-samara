<?php

namespace App\Http\Controllers\Screens;

use App\Layouts\LeftPartActualUser;
use App\Layouts\RightPartActualUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Orchid\Platform\Facades\Alert;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class UpdateUser extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Редактирование клиента';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Редактирование клиента';

    /**
     * Query data
     *
     * @return array
     */
    public function query($id = null): array
    {
        $user = User::find($id);
        return [
            'user' => $user,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Link::name('назад')->link(redirect()->back()->getTargetUrl()),
            Link::name('Удалить')
                ->method('delete'),
            Link::name('Сохранить')
                ->method('save'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            Layouts::columns([
                'left' => [
                    LeftPartActualUser::class,
                ],
                'right' => [
                    RightPartActualUser::class,
                ],
            ]),
        ];
    }

    public function back($request)
    {
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {

        $user = $request->get('user');
        $meta = $user['meta'];
        $meta['no'] = $request->has('user.meta.no');
        $meta['nd'] = $request->has('user.meta.nd');
        $meta['zaselen'] = $request->has('user.meta.zaselen');
        $meta['zaselenme'] = $request->has('user.meta.zaselenme');
        $meta['problematic'] = $request->has('user.meta.problematic');
        $meta['manager_id'] = Auth::user()->id;
        $user_data = [];
        $user_data['meta'] = $meta;
        $user_data['name'] = $meta['last_name'] . ' ' . $meta['first_name'];
        $user_data['email'] = $meta['phone'];
        $user_data['permissions'] = [
            "dashboard.systems.menu" => true,
            "dashboard.index" => true,
        ];

        $meta['password'] = $meta['password'] ?? 'defaultpass';
        $user_data['phone'] = $meta['phone'];
        $user_data['password'] = Hash::make($meta['password']);
        $user_data['manager_id'] = Auth::user()->id;
        $user_data['is_client'] = true;

        $meta['password'] = $meta['password'] ?? 'defaultpass';

        // todo send sms and generate password
        if ($request->has('send_sms')) {
            $smscreq = file_get_contents('https://smsc.ru/sys/send.php?login=julia2018&psw=ione40in&phones=' . urlencode($meta['phone']) . '&charset=utf-8&sender=' . urlencode('your - home') . '&mes=' . urlencode('Ваш пароль ' . $meta['password']));
        }
        $model = User::updateOrCreate([
            'id' => $request->id ?? null,
        ], $user_data);
        Alert::info('Клиент успешно сохранен');
        return redirect()->route('admin.actual-users.index');

    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
        return redirect()->back();
    }
}
