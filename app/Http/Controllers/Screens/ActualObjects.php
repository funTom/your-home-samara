<?php

namespace App\Http\Controllers\Screens;

use App\ActualObjects as ActualObjectsModel;
use App\Http\Filters\ObjectsQueryFilter;
use App\Layouts\ActualObjectsListLayout;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Screen\Screen;

class ActualObjects extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Актуальные объекты';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Актуальные объекты';

    /**
     * Query data
     *
     * @return array
     */
    public function query(): array
    {
        $query = ActualObjectsModel::filters()
            ->filtersApply([
                ObjectsQueryFilter::class,

            ])
            ->with('Type')
            ->with('Manager')
            ->where('archive', false);
        if (Auth::user()->is_client) {
            $query->whereIn('type_id', Auth::user()->meta['types']);
        }
        $objects = $query->defaultSort('updated_at', 'desc')
            ->paginate(50);
        return [
            'actual_objects' => $objects,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            ActualObjectsListLayout::class,
        ];
    }
}
