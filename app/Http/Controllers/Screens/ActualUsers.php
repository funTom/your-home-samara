<?php

namespace App\Http\Controllers\Screens;

use App\Http\Filters\ActualUsersFilter;
use App\Layouts\ActualUsersListLayout;
use App\User;
use Orchid\Platform\Screen\Screen;

class ActualUsers extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Актуальные клиенты';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Актуальные клиенты';

    /**
     * Query data
     *
     * @return array
     */
    public function query(): array
    {
        $actual_users = User::filters()
            ->filtersApply([
                ActualUsersFilter::class,
            ])
            ->where('is_client', true)
            ->defaultSort('updated_at', 'desc')
            ->paginate(50);
        return [
            'actual_users' => $actual_users,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            ActualUsersListLayout::class,
        ];
    }
}
