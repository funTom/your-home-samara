<?php

namespace App\Http\Controllers\Screens;

use App\ActualObjects as ActualObjectsModel;
use App\Http\Filters\ObjectPhoneFilter;
use App\Layouts\ActualObjectsListLayout;
use Orchid\Platform\Screen\Screen;

class ArchiveObjects extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Архивные объекты';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Архивные объекты';

    /**
     * Query data
     *
     * @return array
     */
    public function query(): array
    {
        $objects = ActualObjectsModel::filters()
            ->filtersApply([
                ObjectPhoneFilter::class,
            ])
            ->with('Type')
            ->with('Manager')
            ->where('archive', true)
            ->defaultSort('updated_at', 'desc')
            ->paginate(50);

        return [
            'actual_objects' => $objects,
        ];

    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            ActualObjectsListLayout::class,
        ];
    }
}
