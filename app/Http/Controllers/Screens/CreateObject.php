<?php

namespace App\Http\Controllers\Screens;

use App\ActualObjects;
use App\Layouts\CreateObjectLayout;
use App\Layouts\LeftPart;
use App\Layouts\RightPart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Facades\Alert;
use Orchid\Platform\Screen\Layouts;
use Orchid\Platform\Screen\Link;
use Orchid\Platform\Screen\Screen;

class CreateObject extends Screen
{
    /**
     * Display header name
     *
     * @var string
     */
    public $name = 'Создание объекта';

    /**
     * Display header description
     *
     * @var string
     */
    public $description = 'Создание объекта';

    /**
     * Query data
     *
     * @return array
     */
    public function query($id = null): array
    {
        $object = ActualObjects::with('attachment')->firstOrNew(['id' => $id]);
        if ($object->exists) {
            $this->name = "Редактирование объекта";
            $this->description = "Редактирование объекта";
        }
        return [
            'object' => $object,
            'photos' => $object->attachment,
        ];
    }

    /**
     * Button commands
     *
     * @return array
     */
    public function commandBar(): array
    {
        return [
            Link::name('назад')->link(redirect()->back()->getTargetUrl()),
            Link::name('Сохранить')
                ->method('save'),
        ];
    }

    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function layout(): array
    {
        return [
            Layouts::columns([
                'left' => [
                    LeftPart::class,
                ],
                'right' => [
                    RightPart::class,
                ],
            ]),
            CreateObjectLayout::class,

        ];
    }

    public function back($request)
    {
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $object = $request->get('object');
        $object['furniture'] = $request->has('object.furniture');
        $object['no'] = $request->has('object.no');
        $object['nd'] = $request->has('object.nd');
        $object['archive'] = $request->has('object.archive');
        $object['manager_id'] = Auth::user()->id;

        $model = ActualObjects::updateOrCreate([
            'id' => $request->id ?? null,
        ], $object);
        $model->attachment()
            ->attach($request->get('photos'), [
                'attachmentable_group' => 'photos',
            ]);
        Alert::info('Объект успешно сохранен');
        return redirect()->route('admin.actual-objects.index');
    }
}
