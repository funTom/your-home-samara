<?php
declare (strict_types = 1);

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Orchid\Platform\Http\Controllers\Controller;
use Orchid\Platform\Kernel\Dashboard;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->redirectTo = config('platform.prefix');

        $this->middleware('guest', ['except' => 'logout']);
        $this->dashboard = $dashboard;

    }

    public function username()
    {
        return 'email';
    }

    protected function validateLogin($request)
    {
        $this->validate($request, [
            $this->username() => 'string|email', 'password' => 'required', 'phone' => 'string',
        ]);
    }

    protected function credentials($request)
    {

        if ($request->get('phone')) {
            return ['phone' => $request->get('phone'), 'password' => $request->get('password')];
        }

        return $request->only($this->username(), 'password');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
}
