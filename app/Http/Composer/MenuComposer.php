<?php

namespace App\Http\Composer;

use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Kernel\Dashboard;

class MenuComposer
{
    /**
     * MenuComposer constructor.
     *
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     *
     */
    public function compose()
    {
        $this->dashboard->menu->add('Main', [
            'slug' => 'ActualObjects',
            'icon' => 'icon-browser',
            'route' => '/admin/actual-objects',
            'active' => 'admin.actual-objects.*',
            'childs' => true,
            'main' => true,
            'label' => 'Актуальные объекты',
            'sort' => 99,
        ]);

        $this->dashboard->menu->add('ActualObjects', [
            'slug' => 'ActualObjectsView',
            'icon' => 'icon-browser',
            'route' => '/admin/actual-objects',
            'label' => 'Просмотр',
            'groupname' => 'Актуальные объекты',
            'sort' => 1,
        ]);
        if (!Auth::user()->is_client) {
            $this->dashboard->menu->add('Main', [
                'slug' => 'BlockedPhones',
                'icon' => 'icon-list',
                'route' => '/admin/posts/blocked-phones',
                'active' => 'admin/posts/blocked-phones',
                'main' => true,
                'label' => 'Черный список',
                'sort' => 102,
            ]);

            $this->dashboard->menu->add('Main', [
                'slug' => 'ArchiveObjects',
                'icon' => 'icon-basket',
                'route' => '/admin/archive-objects',
                'active' => 'admin.archive-objects.*',
                'main' => true,
                'label' => 'Архивные объекты',
                'sort' => 100,
            ]);

            $this->dashboard->menu->add('Main', [
                'slug' => 'ActualUsers',
                'icon' => 'icon-user',
                'route' => '/admin/actual-users',
                'active' => 'admin.actual-users.*',
                'main' => true,
                'childs' => true,
                'label' => 'Актуальные клиенты',
                'sort' => 101,
            ]);

            $this->dashboard->menu->add('ActualUsers', [
                'slug' => 'ActualUsersView',
                'icon' => 'icon-browser',
                'route' => '/admin/actual-users',
                'label' => 'Просмотр',
                'groupname' => 'Актуальные клиенты',
                'sort' => 1,
            ]);
        }

        if (Auth::user()->hasAccess('create')) {
            $this->dashboard->menu->add('ActualObjects', [
                'slug' => 'ActualObjectsCreate',
                'icon' => 'icon-browser',
                'route' => '/admin/actual-objects/create',
                'label' => 'Создание',
                'sort' => 2,
            ]);

            $this->dashboard->menu->add('ActualUsers', [
                'slug' => 'ActualUsersCreate',
                'icon' => 'icon-browser',
                'route' => '/admin/actual-users/create',
                'label' => 'Создание',
                'sort' => 2,
            ]);
        }
    }
}
