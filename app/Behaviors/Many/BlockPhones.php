<?php

namespace App\Behaviors\Many;

use App\Http\Filters\BlockedPhonesFilter;
use Orchid\Platform\Behaviors\Many;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Fields\TD;
use Orchid\Platform\Http\Forms\Posts\BasePostForm;
use Orchid\Platform\Http\Forms\Posts\UploadPostForm;

class BlockPhones extends Many
{

    /**
     * @var string
     */
    public $name = 'Черный список';
    public $display = false;

    /**
     * @var string
     */
    public $slug = 'blocked-phones';

    public function options(): array
    {
        return [];
    }

    /**
     * Slug url /news/{name}.
     *
     * @var string
     */
    public $slugFields = 'phone';

    /**
     * Rules Validation.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'sometimes|integer|unique:posts',
            'content.*.phone' => 'required|string',
        ];
    }

    /**
     * HTTP data filters
     *
     * @return array
     */
    public function filters(): array
    {
        return [
            BlockedPhonesFilter::class,
        ];
    }

    public $with = [
        'author',
    ];

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            Field::tag('input')
                ->type('text')
                ->name('phone')
                ->mask('+7 (999) 999-9999')
                ->title('Телефон'),
        ];
    }

    /**
     * Grid View for post type.
     */
    public function grid(): array
    {
        return [
            TD::set('id', 'ID')->linkPost(),
            TD::set('publish_at', 'Дата добавления'),
            TD::set('phone', 'Телефон'),
            TD::set('author', 'Добавил')->setRender(function ($item) {
                return $item->author->name;
            }),
        ];
    }

    /**
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function modules(): array
    {
        return [
            BasePostForm::class,
            UploadPostForm::class,
        ];
    }
}
