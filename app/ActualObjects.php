<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Orchid\Platform\Core\Traits\Attachment;
use Orchid\Platform\Core\Traits\FilterTrait;
use Orchid\Platform\Core\Traits\MultiLanguage;

class ActualObjects extends Model
{
    use FilterTrait;
    use MultiLanguage;
    use Attachment;

    /**
     * @var
     */
    protected $allowedSorts = [
        'id',
        'created_at',
        'fio',
        'price',
    ];

    protected $casts = [
        'photos' => 'collection',
        'address' => 'collection',
    ];

    protected $fillable = [
        'fio',
        'sex',
        'type_id',
        'subtype_id',
        'series',
        'square',
        'price',
        'description',
        'photos',
        'status_id',
        'area_id',
        'address',
        'kv',
        'remonttype_id',
        'manager_id',
        'furniture',
        'phone',
        'phone1',
        'phone2',
        'no',
        'nd',
        'archive',
    ];
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'actual_objects';

    public function Type()
    {
        return $this->belongsTo(Types::class);
    }

    public function Manager()
    {
        return $this->belongsTo(User::class);
    }
}
