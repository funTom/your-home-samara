<?php

namespace App\Layouts;

use App\Areas;
use App\Http\Filters\ActualUsersFilter;
use App\Types;
use App\User;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Fields\TD;
use Orchid\Platform\Layouts\Table;

class ActualusersListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'actual_users';

    /**
     * @return array
     */
    public function filters(): array
    {
        return [
            ActualUsersFilter::class,
        ];
    }

    private function prepareList($Model, $ids)
    {
        $items = $Model::whereIn('id', $ids)->get();
        $data = '';
        foreach ($items as $item) {
            $data = $data . '<br>' . $item->name . ',';
        }
        return $data;
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', 'N')->sort(),

            TD::set('created_at', 'Дата добавления')
                ->sort()
                ->setRender(function ($user) {
                    return date('d.m.Y', strtotime($user->created_at)) . '<br>' . date('H:i', strtotime($user->created_at)) .
                    '<br>' . date('d.m.Y', strtotime($user->updated_at)) . '<br>' . date('H:i', strtotime($user->updated_at));
                }),

            TD::set('meta->last_name', 'ФИО')
                ->setRender(function ($user) {
                    return $user->meta['last_name'] . ' ' . mb_substr($user->meta['first_name'], 0, 1) . ". " . mb_substr($user->meta['middle_name'], 0, 1);
                }),

            TD::set('meta->phone', 'Телефон')
                ->setRender(function ($user) {
                    return $user->meta['phone'];
                }),

            TD::set('meta->areas', 'Район')
                ->setRender(function ($user) {
                    return $this->prepareList(Areas::class, isset($user->meta['areas']) ? $user->meta['areas'] : []);
                }),

            TD::set('meta->types', 'Тип жилья')
                ->setRender(
                    function ($user) {
                        return $this->prepareList(Types::class, $user->meta['types']);
                    }),

            TD::set('meta->manager_id', 'Менеджер')->setRender(function ($user) {
                if (!$user->meta['manager_id']) {
                    return '-';
                }
                $user_id = $user->meta['manager_id'];
                $user = User::find($user_id);
                return $user->name;
            }),

            TD::set('actions', 'Действия')
                ->setRender(function ($user) {
                    $edit_button = '';
                    if (Auth::user()->hasAccess('update')) {
                        $edit_button = '&nbsp;&nbsp;&nbsp;<a href="' . route(
                            'admin.actual-users.edit',
                            $user->id
                        ) . '"><i class="icon-note icons"></i></a>';
                    }
                    return $edit_button;
                }),

        ];

    }
}
