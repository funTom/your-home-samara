<?php
namespace App\Layouts;

use App\Subtypes;
use App\Types;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class LeftPartView extends Rows
{

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->name('object.fio')
                        ->title('ФИО')
                        ->disabled()
                        ->required(),
                    Field::tag('select')
                        ->options([
                            1 => 'Мужской',
                            0 => 'Женский',
                        ])
                        ->required()
                        ->disabled()
                        ->name('object.sex')
                        ->modifyValue(function () {
                            return (int) $this->query->getContent('object.sex');
                        })
                        ->title('Пол'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Types::class))
                        ->disabled()
                        ->name('object.type_id')
                        ->required()
                        ->title('Тип недвижимости'),
                    Field::tag('select')
                        ->options($this->prepareSelect(Subtypes::class))
                        ->disabled()
                        ->name('object.subtype_id')
                        ->title('Подтип недвижимости'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->disabled()
                        ->name('object.series')
                        ->title('Серия'),
                    Field::tag('input')
                        ->type('number')
                        ->disabled()
                        ->name('object.square')
                        ->title('Площадь'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->disabled()
                        ->type('number')
                        ->required()
                        ->name('object.price')
                        ->title('Цена'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('textarea')
                        ->disabled()
                        ->rows(5)
                        ->name('object.description')
                        ->title('Описание'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('checkbox')
                        ->name('object.no')
                        ->disabled()
                        ->title('Н.О.'),
                    Field::tag('checkbox')
                        ->name('object.nd')
                        ->disabled()
                        ->title('Н.Д.'),
                    Field::tag('checkbox')
                        ->name('object.archive')
                        ->disabled()
                        ->title('Сдана'),
                ];
            }),
        ];
    }
}
