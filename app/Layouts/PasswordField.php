<?php
namespace App\Layouts;

use Orchid\Platform\Fields\Field;

class PasswordField extends Field
{

    public $view = 'password';

    /**
     * Required Attributes.
     *
     * @var array
     */
    public $required = [
        'name',
        'title',
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    public $inlineAttributes = [
        'name',
        'title',
    ];
}
