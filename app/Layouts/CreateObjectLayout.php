<?php

namespace App\Layouts;

use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Screen\Layouts;

class CreateObjectLayout extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::tag('upload')
                ->name('photos')
                ->disabled()
                ->title('Фото'),
        ];
    }
}
