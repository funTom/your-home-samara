<?php
namespace App\Layouts;

use App\Areas;
use App\Layouts\PasswordField;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class RightPartActualUser extends Rows
{

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->name('user.meta.dogovor_number')
                        ->title('Номер договора'),
                    Field::tag('input')
                        ->name('user.meta.dogovor_series')
                        ->title('Серия'),
                    Field::tag('input')
                        ->type('number')
                        ->name('user.meta.dogovor_price')
                        ->title('Цена договора'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->type('number')
                        ->name('user.meta.rehash_price')
                        ->title('Цена рехеша (если есть)'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('datetime')
                        ->type('text')
                        ->name('user.meta.dop_platezh_date')
                        ->title('Дата доп. платежа'),

                    Field::tag('input')
                        ->type('number')
                        ->name('user.meta.dop_platezh_price')
                        ->title('Сумма доп. платежа'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options([
                            "Интернет" => "Интернет",
                            "Авито" => "Авито",
                            "Вывеска" => "Вывеска",
                            "Визитка" => "Визитка",
                            "Рекомендации" => "Рекомендации",
                            "Сайт" => "Сайт",
                            "Из рук в руки-сайт" => "Из рук в руки-сайт",
                            "Из рук в руки-газета" => "Из рук в руки-газета",
                            "Вконтакте-группа" => "Вконтакте-группа",
                            "Вконтакте-реклама" => "Вконтакте-реклама",
                            "Майл.ru" => "Майл.ru",
                            "Фейсбук" => "Фейсбук",
                            "E-mail" => "E-mail",
                            "СМС" => "СМС",

                        ])
                        ->name('user.meta.source')
                        ->title('Источник'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Areas::class))
                        ->multiple()
                        ->name('user.meta.areas.')
                        ->modifyValue(function ($user) {
                            return $this->query->getContent('user.meta.areas');
                        })
                        ->title('Район'),
                ];
            }),
            Field::group(function () {
                $pass_field = new PasswordField;
                return [
                    Field::tag('input')
                        ->type('text')
                        ->name('user.meta.phone')
                        ->required()
                        ->mask('+7 (999) 999-9999')
                        ->title('Телефон'),

                    $pass_field
                        ->required()
                        ->name('user.meta.password')
                        ->title('Пароль'),

                    Field::tag('checkbox')
                        ->name('send_sms')
                        ->title('Отправить пароль клиенту'),
                ];
            }),
        ];
    }
}
