<?php
namespace App\Layouts;

use Orchid\Platform\Fields\Field;

class ImagesField extends Field
{

    public $view = 'images';

    /**
     * Required Attributes.
     *
     * @var array
     */
    public $required = [
        'name',
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    public $inlineAttributes = [
        'name',
    ];
}
