<?php
namespace App\Layouts;

use App\Areas;
use App\Remonttypes;
use App\Statuses;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class RightPart extends Rows
{

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Statuses::class))
                        ->name('object.status_id')
                        ->title('Статус'),
                    Field::tag('select')
                        ->options($this->prepareSelect(Areas::class))
                        ->name('object.area_id')
                        ->title('Район'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('place')
                        ->name('object.address')
                        ->title('Адрес'),

                    Field::tag('input')
                        ->name('object.housenum')
                        ->title('№ квартиры'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Remonttypes::class))
                        ->name('object.remonttype_id')
                        ->title('Ремонт'),

                    Field::tag('checkbox')
                        ->value(true)
                        ->name('object.furniture')
                        ->title('Мебель'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->type('text')
                        ->name('object.phone')
                        ->mask('+7 (999) 999-9999')
                        ->title('Телефон'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->type('text')
                        ->name('object.phone1')
                        ->mask('+7 (999) 999-9999')
                        ->title('Доп. Телефон 1'),
                    Field::tag('input')
                        ->type('text')
                        ->name('object.phone2')
                        ->mask('+7 (999) 999-9999')
                        ->title('Доп. Телефон 2'),
                ];
            }),
        ];
    }
}
