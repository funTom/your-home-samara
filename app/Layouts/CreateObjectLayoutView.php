<?php

namespace App\Layouts;

use Orchid\Platform\Layouts\Rows;
use Orchid\Platform\Screen\Layouts;

class CreateObjectLayoutView extends Rows
{
    /**
     * Views

     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        $field = new ImagesField;
        $field->name('photos');
        return [
            $field,
        ];
    }
}
