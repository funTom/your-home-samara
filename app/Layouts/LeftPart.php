<?php
namespace App\Layouts;

use App\Subtypes;
use App\Types;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class LeftPart extends Rows
{

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->name('object.fio')
                        ->title('ФИО')
                        ->required(),
                    Field::tag('select')
                        ->options([
                            1 => 'Мужской',
                            0 => 'Женский',
                        ])
                        ->required()
                        ->name('object.sex')
                        ->modifyValue(function () {
                            return (int) $this->query->getContent('object.sex');
                        })
                        ->title('Пол'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Types::class))
                        ->name('object.type_id')
                        ->required()
                        ->title('Тип недвижимости'),
                    Field::tag('select')
                        ->options($this->prepareSelect(Subtypes::class))
                        ->name('object.subtype_id')
                        ->title('Подтип недвижимости'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->name('object.series')
                        ->title('Серия'),
                    Field::tag('input')
                        ->type('number')
                        ->name('object.square')
                        ->title('Площадь'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->type('number')
                        ->required()
                        ->name('object.price')
                        ->title('Цена'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('textarea')
                        ->rows(5)
                        ->name('object.description')
                        ->title('Описание'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('checkbox')
                        ->name('object.no')
                        ->title('Н.О.'),
                    Field::tag('checkbox')
                        ->name('object.nd')
                        ->title('Н.Д.'),
                    Field::tag('checkbox')
                        ->name('object.archive')
                        ->title('Сдана'),
                ];
            }),
        ];
    }
}
