<?php

namespace App\Layouts;

use App\Http\Filters\ObjectsQueryFilter;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Fields\TD;
use Orchid\Platform\Layouts\Table;

class ActualObjectsListLayout extends Table
{

    /**
     * @var string
     */
    public $data = 'actual_objects';

    /**
     * @return array
     */
    public function filters(): array
    {
        return [
            ObjectsQueryFilter::class,
        ];
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', 'N')->sort(),

            TD::set('created_at', 'Дата добавления')
                ->sort()
                ->setRender(function ($object) {
                    return date('d.m.Y', strtotime($object->created_at)) . '<br>' . date('H:i', strtotime($object->created_at)) .
                    '<br>' . date('d.m.Y', strtotime($object->updated_at)) . '<br>' . date('H:i', strtotime($object->updated_at));
                }),

            TD::set('address', 'Объект')
                ->sort()
                ->width(180)
                ->setRender(function ($object) {
                    return $object->address['name'] . ',<br> ' . $object->fio .
                        '<br>' . ($object->no ? '<b class="text-danger">Н.О.</b>' : '') .
                        '<br>' . ($object->nd ? '<b class="text-danger">Н.Д.</b>' : '');
                }),

            TD::set('furniture', 'Мебель')
                ->sort()
                ->setRender(function ($object) {
                    return $object->furniture ? 'Да' : 'Нет';
                }),

            TD::set('price', 'Цена')->sort(),

            TD::set('type_id', 'Тип')
                ->setRender(function ($object) {
                    return $object->type->name ?? '-';
                }),

            TD::set('manager_id', 'Менеджер')
                ->setRender(function ($object) {
                    return $object->Manager->name ?? '-';
                }),

            TD::set('actions', 'Действия')
                ->setRender(function ($object) {
                    $edit_button = '';
                    if (Auth::user()->hasAccess('update')) {
                        $edit_button = '&nbsp;&nbsp;&nbsp;<a href="' . route(
                            'admin.actual-objects.edit',
                            $object->id
                        ) . '"><i class="icon-note icons"></i></a>';
                    }
                    $view_button = '<a href="' . route(
                        'admin.actual-objects.view',
                        $object->id
                    ) . '"><i class="icon-info icons"></i></a>';
                    return $view_button . $edit_button;
                }),

        ];

    }
}
