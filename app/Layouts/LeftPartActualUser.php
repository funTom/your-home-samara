<?php
namespace App\Layouts;

use App\Types;
use Orchid\Platform\Fields\Field;
use Orchid\Platform\Layouts\Rows;

class LeftPartActualUser extends Rows
{

    private function prepareSelect($Model)
    {
        $items = $Model::all();
        $data = [];
        foreach ($items as $item) {
            $data[$item->id] = $item->name;
        }
        return $data;
    }

    private function prepareName($data)
    {
        dd($data);
        // $items = $Model::all();
        // $data = [];
        // foreach ($items as $item) {
        //     $data[$item->id] = $item->name;
        // }
        // return $data;
    }
    /**
     * Views
     *
     * @return array
     * @throws \Orchid\Platform\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->name('user.meta.last_name')
                        ->title('Фамилия')
                        ->required(),
                    Field::tag('input')
                        ->name('user.meta.first_name')
                        ->title('Имя')
                        ->required(),
                    Field::tag('input')
                        ->name('user.meta.middle_name')
                        ->title('Отчество'),
                    Field::tag('select')
                        ->options([
                            1 => 'Мужской',
                            0 => 'Женский',
                        ])
                        ->required()
                        ->name('user.meta.sex')
                        ->modifyValue(function () {
                            return (int) $this->query->getContent('meta.sex');
                        })
                        ->title('Пол'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('select')
                        ->options($this->prepareSelect(Types::class))
                        ->multiple()
                        ->name('user.meta.types.')
                        ->modifyValue(function ($user) {
                            return $this->query->getContent('user.meta.types');
                        })
                        ->required()
                        ->title('Тип недвижимости'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('input')
                        ->type('number')
                        ->required()
                        ->name('user.meta.priceFrom')
                        ->title('Цена от'),
                    Field::tag('input')
                        ->type('number')
                        ->required()
                        ->name('user.meta.priceTo')
                        ->title('Цена до'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('textarea')
                        ->rows(5)
                        ->name('user.meta.comment')
                        ->title('Комментарий'),
                ];
            }),

            Field::group(function () {
                return [
                    Field::tag('checkbox')
                        ->name('user.meta.no')
                        ->title('Н.О.'),
                    Field::tag('checkbox')
                        ->name('user.meta.nd')
                        ->title('Н.Д.'),
                ];
            }),
            Field::group(function () {
                return [
                    Field::tag('checkbox')
                        ->name('user.meta.zaselen')
                        ->title('Заселен'),
                    Field::tag('checkbox')
                        ->name('user.meta.zaselenme')
                        ->title('Заселен через нас'),
                    Field::tag('checkbox')
                        ->name('user.meta.problematic')
                        ->title('Проблемный'),
                ];
            }),
        ];
    }
}
