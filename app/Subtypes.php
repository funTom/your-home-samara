<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtypes extends Model
{
    protected $table = 'subtypes';
}
