<?php

namespace App\Providers;

use App\Http\Composer\MenuComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Orchid\Platform\Kernel\Dashboard;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dashboard $dashboard)
    {
        View::composer('dashboard::layouts.dashboard', MenuComposer::class);
        $dashboard->registerPermissions([
            'Объекты' => [
                [
                    'slug' => 'create',
                    'description' => 'Создание',
                ],
                [
                    'slug' => 'update',
                    'description' => 'Изменение',
                ],
                [
                    'slug' => 'delete',
                    'description' => 'Удаление',
                ],
            ],
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
