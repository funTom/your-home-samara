<?php

use App\ActualObjects;
use App\Areas;
use App\Remonttypes;
use App\Types;
use Illuminate\Database\Seeder;
use Orchid\Platform\Core\Models\Attachment;
use Orchid\Platform\Core\Models\Attachmentable;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Types::create([
            'id' => 1,
            'name' => '1-ком квартира',
        ]);
        Types::create([
            'id' => 2,
            'name' => '2-ком квартира',
        ]);
        Types::create([
            'id' => 3,
            'name' => '3-ком квартира',
        ]);
        Types::create([
            'id' => 4,
            'name' => '4-ком и более квартира',
        ]);
        Types::create([
            'id' => 5,
            'name' => 'Гостинка',
        ]);
        Types::create([
            'id' => 6,
            'name' => 'Койко-место',
        ]);
        Types::create([
            'id' => 7,
            'name' => 'Коммуналка',
        ]);
        Types::create([
            'id' => 8,
            'name' => 'Комната',
        ]);
        Types::create([
            'id' => 9,
            'name' => 'Коттедж',
        ]);
        Types::create([
            'id' => 10,
            'name' => 'Малосемейка',
        ]);
        Types::create([
            'id' => 11,
            'name' => 'Общежитие',
        ]);
        Types::create([
            'id' => 12,
            'name' => 'Частный дом',
        ]);
        Types::create([
            'id' => 13,
            'name' => 'Часть дома',
        ]);
        Types::create([
            'id' => 14,
            'name' => 'Студия',
        ]);
        Types::create([
            'id' => 15,
            'name' => 'ОФОРМИЛСЯ !',
        ]);

        Remonttypes::create([
            'id' => 1,
            'name' => 'Обычный',
        ]);
        Remonttypes::create([
            'id' => 2,
            'name' => 'Евро ремонт',
        ]);

        Areas::create([
            'id' => 1,
            'name' => 'Куйбышевский',
        ]);
        Areas::create([
            'id' => 2,
            'name' => 'Самарский',
        ]);
        Areas::create([
            'id' => 3,
            'name' => 'Ленинский',
        ]);
        Areas::create([
            'id' => 4,
            'name' => 'Железнодорожный',
        ]);
        Areas::create([
            'id' => 5,
            'name' => 'Октябрьский',
        ]);
        Areas::create([
            'id' => 6,
            'name' => 'Советский',
        ]);
        Areas::create([
            'id' => 7,
            'name' => 'Промышленный',
        ]);
        Areas::create([
            'id' => 8,
            'name' => 'Кировский',
        ]);
        Areas::create([
            'id' => 9,
            'name' => 'Красноглинский',
        ]);

        function formatDataItem(array $item = [])
        {
            $photos = $item['parsedData']['photos'] ?? [];
            foreach ($photos as $photo) {
                $ext = 'jpeg';
                $name = basename($photo, '.' . $ext);
                $original_name = basename($photo);
                $mime = 'image/jpeg';
                $path = 'photos/';
                $attachment = Attachment::create([
                    'name' => $name,
                    'original_name' => $original_name,
                    'mime' => $mime,
                    'extension' => $ext,
                    'path' => $path,
                ]);
                Attachmentable::create([
                    'attachmentable_type' => 'App\ActualObjects',
                    'attachmentable_id' => $item['item']['obj_id'],
                    'attachment_id' => $attachment->id,
                    'attachmentable_group' => 'photos',
                ]);
            }

            $data = [
                'id' => $item['item']['obj_id'],
                'price' => $item['item']['repair'],
                'fio' => $item['parsedData']['fio'],
                'sex' => $item['parsedData']['sex'] == "Мужской" ? true : false,
                'address' => [
                    'name' => $item['parsedData']['street'],
                    'lat' => 0,
                    'lng' => 0,
                ],
                'description' => $item['parsedData']['description'],
                'phone' => $item['parsedData']['phone'],
                'phone1' => $item['parsedData']['subphone1'],
                'phone2' => $item['parsedData']['subphone2'],
                'square' => $item['parsedData']['square'] || null,
                'type_id' => Types::where('name', $item['parsedData']['type'])->first()->id,
                'subtype_id' => null,
                'series' => $item['parsedData']['series'],
                'status_id' => null,
                'kv' => $item['parsedData']['kv'] || null,
                'area_id' => $item['parsedData']['area'] && Areas::where('name', $item['parsedData']['area'])->first()->id,
                'remonttype_id' => $item['parsedData']['remonttype'] && Remonttypes::where('name', $item['parsedData']['remonttype'])->first()->id,
                'furniture' => $item['item']['furniture'] == "Да",
                'no' => strpos($item['item']['object'], "Н.О.") !== false,
                'nd' => strpos($item['item']['object'], "Н.Д.") !== false,
            ];
            ActualObjects::create($data);
        }

        $str = file_get_contents('./result10.json');
        $json = json_decode($str, true);
        array_map("formatDataItem", $json);
    }
}
