<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
        });

        Schema::create('subtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
        });

        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
        });

        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
        });

        Schema::create('remonttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('name');
        });

        Schema::create('actual_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fio');
            $table->boolean('sex');
            $table->float('price', 8, 2);
            $table->jsonb('address');
            $table->jsonb('photos')->nullable();
            $table->text('description')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->unsignedInteger('type_id')->nullable(); // связь
            $table->unsignedInteger('subtype_id')->nullable(); // связь
            $table->string('series')->nullable();
            $table->float('square', 8, 2)->nullable();
            $table->unsignedInteger('status_id')->nullable(); // связь
            $table->unsignedInteger('manager_id')->nullable(); // связь
            $table->unsignedInteger('area_id')->nullable(); // связь
            $table->string('kv')->nullable()->nullable();
            $table->unsignedInteger('remonttype_id')->nullable(); // связь
            $table->boolean('furniture')->nullable();
            $table->boolean('no')->nullable();
            $table->boolean('nd')->nullable();
            $table->boolean('archive')->default(false);

            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('subtype_id')->references('id')->on('subtypes');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('area_id')->references('id')->on('areas');
            $table->foreign('manager_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_objects');
        Schema::dropIfExists('types');
        Schema::dropIfExists('subtypes');
        Schema::dropIfExists('statuses');
        Schema::dropIfExists('areas');
        Schema::dropIfExists('remonttypes');
    }

}
