<div>
  @foreach ($value as $photo)
   <a href="/storage/{{$photo->path}}{{$photo->original_name}}" target="_blank" style="display:inline-block; width: 200px; height: auto; margin: 5px;">
     <img src="/storage/{{$photo->path}}{{$photo->original_name}}" style="width: 100%;"/>
</a>
  @endforeach

</div>

<style>
  .form-control[disabled], .chosen-choices[disabled], .chosen-single[disabled], .bootstrap-tagsinput[disabled], .form-control[readonly], .chosen-choices[readonly], .chosen-single[readonly], .bootstrap-tagsinput[readonly], fieldset[disabled] .form-control, fieldset[disabled] .chosen-choices, fieldset[disabled] .chosen-single, fieldset[disabled] .bootstrap-tagsinput, .form-control-grey[disabled], .form-control-grey[readonly], fieldset[disabled] .form-control-grey {
    color: rgba(73, 80, 87, 0.83);
  }
</style>
