<?php
use App\Areas;
use App\Remonttypes;
use App\Types;
use App\User;
use Orchid\Platform\Fields\Field;

function prepareSelect($Model)
{
    $items = $Model::all();
    $data = [
        '' => 'Не выбрано',
    ];
    foreach ($items as $item) {
        $data[$item->id] = $item->name;
    }
    return $data;
}

$booleanOptions = [
    '' => 'Не выбрано',
    '0' => 'Нет',
    '1' => 'Да',
];
?>
<div id="tododeleteit">

</div>
<a class="btn btn-default" data-toggle="collapse" href="#collapseFlter" role="button" aria-expanded="false" aria-controls="collapseExample">
    Расширенный фильтр
  </a>
  <?=Field::tag('input')
->value($request->get('search'))
->form('filters')
->name('search')
->title('Поиск')->render();?>
  <div class="col-md-12 collapse" id="collapseFlter">
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class="col-md">
            <?=Field::tag('select')
->value($request->get('type_id') ? intval($request->get('type_id')) : null)
->form('filters')
->options(prepareSelect(Types::class))
->name('type_id')
->title('Тип')->render();?>
          </div>
          <div class="col-md">
            <?=Field::tag('input')
->value($request->get('price_from'))
->form('filters')
->type('number')
->name('price_from')
->title('Цена от')->render();?>
          </div>
          <div class="col-md">
            <?=Field::tag('input')
->value($request->get('price_to'))
->form('filters')
->type('number')
->name('price_to')
->title('Цена до')->render();?>
          </div>
        </div>
        <div class="row">
          <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('series'))
->form('filters')
->name('series')
->title('Серия')->render();
?>
          </div>
          <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('square_from'))
->form('filters')
->type('number')
->name('square_from')
->title('Площадь от')->render();
?>
          </div>
          <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('square_to'))
->form('filters')
->type('number')
->name('square_to')
->title('Площадь до')->render();
?>
          </div>
        </div>
        <div class="row">
          <div class="col-md">
            <?=
Field::tag('select')
->value($request->get('furniture') ? intval($request->get('furniture')) : null)
->form('filters')
->options($booleanOptions)
->name('furniture')
->title('С мебелью')->render();

?>
          </div>
          <div class="col-md">
            <?=
Field::tag('select')
->value($request->get('remonttype_id') ? intval($request->get('remonttype_id')) : null)
->form('filters')
->options(prepareSelect(Remonttypes::class))
->name('remonttype_id')
->title('Ремонт')->render();?>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md">
            <?=
Field::tag('select')
->value($request->get('area_id') ? intval($request->get('area_id')) : null)
->form('filters')
->options(prepareSelect(Areas::class))
->name('area_id')
->title('Район')->render();?>
          </div>
          <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('phone'))
->name('phone')
->form('filters')
->mask('+7 (999) 999-9999')
->title('Номер телефона')->render();
?>
          </div>
        </div>
        <div class="row">
          <div class="col-md">
            <?=
Field::tag('select')
->value($request->get('manager_id') ? intval($request->get('manager_id')) : null)
->form('filters')
->options(prepareSelect(User::class))
->name('manager_id')
->title('Менеджер')->render();?>
          </div>
          <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('id'))
->form('filters')
->name('id')
->title('Номер объекта')->render();
?>
          </div>
        </div>
        <div class="row">
          <div class="col-md">

            <?=
Field::tag('select')
->value($request->get('no') ? intval($request->get('no')) : null)
->form('filters')
->options($booleanOptions)
->name('no')
->title('Н.О.')->render();

?>
          </div>
          <div class="col-md">
            <?=
Field::tag('select')
->value($request->get('nd') ? intval($request->get('nd')) : null)
->form('filters')
->options($booleanOptions)
->name('nd')
->title('Н.Д.')->render();
?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    document.addEventListener('turbolinks:load', () => {
      $('#tododeleteit').parent().removeClass('col-sm-3');
    })
  </script>