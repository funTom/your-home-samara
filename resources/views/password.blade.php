<style>
  select.form-control[multiple] {
    height: 100px !important;
}
</style>

<div class="form-group">
  <label for="field--{{$name}}">{{$title}}
  </label>

  <div class="input-group">
    <div class="input-group-prepend" id="generate-pass--{{$name}}">
      <i class="input-group-text icon-certificate" id="inputGroupPrepend">*</i>
    </div>
    <input class="form-control" type="text" required  value="{{$value}}" name="{{$name}}" id="field--{{$name}}" title="{{$title}}">
  </div>
</div>
<div class="line line-dashed b-b line-lg"></div>

<script>
  function makePass() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  document.getElementById("generate-pass--{{$name}}").onclick = function () {
    var pass = makePass()
    document.getElementById("field--{{$name}}").value = pass
  }
</script>
