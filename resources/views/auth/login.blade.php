@extends('dashboard::layouts.auth')
@section('content')
    <p class="m-t-lg">{{trans('dashboard::auth/account.title')}}</p>
    <form class="m-t-md" role="form" method="POST" action="{{  route('login') }}">
        @csrf
        <div class="form-group form-group-default">
          <div class="col-12">
                <div class="checkbox">
                    <label class="i-checks">
                        <input id="login-select" type="checkbox"
                               name="remember"><i></i> Войти по номеру телефона
                    </label>
                </div>
            </div>
        </div>
        <div id="login-wrapper">

        </div>
        <div class="form-group form-group-default {{ $errors->has('password') ? ' has-error' : '' }}">
            <label>{{trans('dashboard::auth/account.password')}}</label>
            <div class="controls">
                <input type="password" class="form-control" name="password"
                       placeholder="{{trans('dashboard::auth/account.enter_password')}}" required>
                @if ($errors->has('password'))
                    <span class="form-text text-muted">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row v-center padder-v">
            <div class="col-5">
                <div class="checkbox">
                    <label class="i-checks">
                        <input type="checkbox" checked
                               name="remember"><i></i> {{trans('dashboard::auth/account.remember_me')}}
                    </label>
                </div>
            </div>
            <div class="col-7 text-right">
                <a href="{{ route('password.request') }}"
                   class="text-primary small">{{trans('dashboard::auth/account.forgot_password')}}</a>
            </div>
        </div>
        <button class="btn btn-primary m-t-md" type="submit" dusk="login-button">
            <i class="icon-login text-xs m-r-xs"></i> {{trans('dashboard::auth/account.login')}}
        </button>
    </form>
<script id="login-phone" type="text/template">
  <div class="form-group form-group-default {{ $errors->has('phone') ? ' has-error' : '' }}">
      <label>Телефон</label>
      <div class="controls">
          <input name="phone" placeholder="Введите номер телефона"
                  class="form-control" required
                  value="{{ old('phone') }}"
                  data-mask="+7 (999) 999-9999">
          @if ($errors->has('phone'))
              <span class="form-text text-muted">
                  <strong>{{ $errors->first('phone') }}</strong>
              </span>
          @endif
      </div>
      <script>
          console.log("asfasfa")
         $('input[data-mask]').each(function () {
      Inputmask($(this).data('mask')).mask($(this));
    });
</script>
  </div>
</script>
<script id="login-email" type="text/template">
    <div id="login-email" class="form-group form-group-default {{ $errors->has('email') ? ' has-error' : '' }}">
            <label>Email</label>
            <div class="controls">
                <input type="email" name="email" placeholder="{{trans('dashboard::auth/account.enter_email')}}"
                       class="form-control" required
                       value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="form-text text-muted">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
</script>
<script>
  window.onload = function() {
    var template = $('#login-email');
    $('#login-wrapper').html(template.html())

    $('#login-select').change(function(event) {
      var value = $(this).is(':checked');
      if(value) {
        var template = $('#login-phone');
        $('#login-wrapper').html(template.html())
        return
      }
      var template = $('#login-email');
      $('#login-wrapper').html(template.html())
    });

   };
</script>
@endsection
