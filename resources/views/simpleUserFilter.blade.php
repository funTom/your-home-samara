<?php
use App\User;
use Orchid\Platform\Fields\Field;

function prepareUsers($Model)
{
    $items = $Model::where('is_client', false)->orWhereNull('is_client')->get();
    $data = [];
    foreach ($items as $item) {
        $data[$item->id] = $item->name;
    }
    return $data;
}

$booleanOptions = [
    '' => 'Не выбрано',
    '0' => 'Нет',
    '1' => 'Да',
];
?>
  <div id="tododeleteit">

  </div>
  <a class="btn btn-default" data-toggle="collapse" href="#collapseFlter" role="button" aria-expanded="false" aria-controls="collapseExample">
    Расширенный фильтр
  </a>
  <?=Field::tag('input')
->value($request->get('search'))
->form('filters')
->name('search')
->title('Поиск')->render();?>
    <div class="col-md-12 collapse" id="collapseFlter">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md">
              <?=Field::tag('input')
->value($request->get('dogovor_series'))
->form('filters')
->name('dogovor_series')
->title('Серия договора')->render();?>
            </div>
            <div class="col-md">
              <?=Field::tag('input')
->value($request->get('dogovor_number'))
->form('filters')
->name('dogovor_number')
->title('Номер договора')->render();?>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
            <?=
Field::tag('input')
->value($request->get('phone'))
->form('filters')
->name('phone')
->mask('+7 (999) 999-9999')
->title('Номер телефона')->render();
?>
          </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md">
              <?=
Field::tag('select')
->value($request->get('zaselen') ? intval($request->get('zaselen')) : null)
->form('filters')
->options($booleanOptions)
->name('zaselen')
->title('Заселен')->render();

?>
            </div>
            <div class="col-md">
              <?=
Field::tag('select')
->value($request->get('zaselenme') ? intval($request->get('zaselenme')) : null)
->form('filters')
->options($booleanOptions)
->name('zaselenme')
->title('Заселен через нас')->render();

?>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <?=
Field::tag('select')
->value($request->get('manager_id') ? $request->get('manager_id') : [])
->form('filters')
->options(prepareUsers(User::class))
->multiple()
->name('manager_id[]')
->title('Менеджер')->render();?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script>
      document.addEventListener('turbolinks:load', () => {
        $('#tododeleteit').parent().removeClass('col-sm-3');
        $('#tododeleteit').parent().addClass('col-sm-12');
      })
    </script>
    <style>
      select.form-control[multiple] {
        height: 100px !important;
      }
    </style>